package uberalltest;

import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Logger;

public class MyCSVWriter {


    public static String addRow(Object obj, char separator) {
        StringBuilder csvRow = new StringBuilder();
        Field[] fields = obj.getClass().getDeclaredFields();
        boolean firstField = true;
        for (Field field : fields) {
            field.setAccessible(true);
            Object value;
            String v;
            try {
                value = field.get(obj);
                v = String.valueOf(value);
                if(value == null){
                    v = "";
                }
                if (firstField) {
                    csvRow.append(v);
                    firstField = false;
                }
                else {
                    csvRow.append(separator).append(v);
                }
                field.setAccessible(false);
            } catch (IllegalArgumentException | IllegalAccessException e) {
               e.printStackTrace();
            }
        }
        return csvRow.toString();
    }

    public static String convertToCSV(Object object){
        StringWriter stringWriter = new StringWriter();
        if(object instanceof List<?>) {
            for (Object l : ((List<?>) object)) {

                String row = MyCSVWriter.addRow(l, ',');
                System.out.println("=======:==============>" + row);
                stringWriter.append(row).append("\n");
            }
        }else{
            String row = MyCSVWriter.addRow(object, ',');
            stringWriter.append(row).append("\n");
        }
        System.out.println("------------------>"+stringWriter.toString());
        return stringWriter.toString();
    }


}
