package uberalltest

class LocationToCSVWriter {
    static List<LocationDTO> convertToLocation(List<Location> locations){
        List<LocationDTO> dataModels = new ArrayList<>()
        for(Location l : locations){
            LocationDTO dm = new LocationDTO(city: l.getCity(),name: l.getName(),zip: l.getZip(),streetAndNumber: l.getStreetAndNumber()
                    ,lat: l.getLat(),lng: l.getLng(),openingHours: l.getOpeningHours(),keywords: l.getKeywords())
            dataModels.add(dm)
        }
        return dataModels
    }

}
