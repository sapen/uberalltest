package uberalltest

import grails.gorm.transactions.Transactional

@Transactional
class LocationService {
    def list() {
        Location.list()
    }
    def addAll(ArrayList<Location> locations){
            Location.saveAll(locations)
    }

    def add(Location location){
        location.save()
    }
}
