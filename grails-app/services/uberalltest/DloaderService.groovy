package uberalltest

import grails.gorm.transactions.Transactional
import org.grails.core.io.ResourceLocator
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired


@Transactional
class DloaderService {


    @Autowired
    LocationService locationService

    @Autowired
    ResourceLocator grailsResourceLocator

    def loadDataApi() {
        def resource = this.class.classLoader.getResourceAsStream('test.json')
        String all = resource.text
        JSONObject obj = new JSONObject(all)
        JSONArray arr = new JSONArray(obj.getJSONObject("response").getJSONArray("locations"))
        for (i in 0..<arr.length()) {
            JSONObject objLoc = arr.getJSONObject(i)
            def location = new Location(city: objLoc.getString("city") as String, lat: objLoc.getString("lat") as BigDecimal,
            lng: objLoc.getString("lng") as BigDecimal,name: objLoc.getString("name") as String , zip: objLoc.getString("zip") as String
            , streetAndNumber: objLoc.getString("streetAndNumber") as String, openingHours: objLoc.getString("openingHours") as String
            , keywords: objLoc.getString("keywords") as String)
            locationService.add(location)
        }
    }
}
