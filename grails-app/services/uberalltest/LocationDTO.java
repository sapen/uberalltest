package uberalltest;

import java.math.BigDecimal;

public class LocationDTO {
    public String name;
    public String city;
    public String zip;
    public String streetAndNumber;
    public String openingHours;
    public String keywords;
    public BigDecimal lat;
    public BigDecimal lng;

    public LocationDTO() {
    }

    public LocationDTO(String name, String city, String zip, String streetAndNumber, String openingHours, String keywords, BigDecimal lat, BigDecimal lng) {
        this.name = name;
        this.city = city;
        this.zip = zip;
        this.streetAndNumber = streetAndNumber;
        this.openingHours = openingHours;
        this.keywords = keywords;
        this.lat = lat;
        this.lng = lng;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStreetAndNumber() {
        return streetAndNumber;
    }

    public void setStreetAndNumber(String streetAndNumber) {
        this.streetAndNumber = streetAndNumber;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", streetAndNumber='" + streetAndNumber + '\'' +
                ", openingHours='" + openingHours + '\'' +
                ", keywords='" + keywords + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
