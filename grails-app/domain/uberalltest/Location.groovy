package uberalltest

import javax.persistence.Column
import javax.persistence.Lob


class Location {

    String name;
    String city;
    String zip;
    String streetAndNumber;
    @Lob
    String openingHours;
    String keywords;
    BigDecimal lat;
    BigDecimal lng;
    static mapping = {
        openingHours type: 'text'
    }

}
