package uberalltest

import grails.converters.JSON
import grails.converters.XML

class LocationController {
    LocationService locationService;
    DloaderService dloaderService;


    def list(){
        List locations = locationService.list();
        if(locations.isEmpty()){
            dloaderService.loadDataApi()
            locations = locationService.list();
        }

        withFormat {
            json { render locations as JSON }
            xml { render locations as XML }
            csv {
                String result = MyCSVWriter.convertToCSV(LocationToCSVWriter.convertToLocation(locations))
                render(contentType:'text/csv',text:result)
            }
            '*' {
                render locations as JSON
            }

        }
    }
}
